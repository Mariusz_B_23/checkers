package moves;


import exceptions.CheckerIsNullException;
import exceptions.ColorGroupCannotBeNullOrEmptyException;
import exceptions.CoordinateAreNullException;
import game.Board;
import game.Coordinate;
import piece.Checker;

import java.util.ArrayList;
import java.util.List;

public class QueenMove implements Move {

    private static final Board BOARD = Board.getInstance();


    public Checker getPieceThatTheQueenHasKnockedDown(Checker selectedChecker, Coordinate coordinate) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (coordinate == null) {
            throw new CoordinateAreNullException("Coordinate cannot be null!");
        }
        int xDirection = Integer.compare(coordinate.getX(), selectedChecker.getCoordinate().getX());
        int yDirection = Integer.compare(coordinate.getY(), selectedChecker.getCoordinate().getY());
        int xDiagonally = (selectedChecker.getCoordinate().getX()) + (xDirection);
        int yDiagonally = (selectedChecker.getCoordinate().getY()) + (yDirection);
        int xBehindKnockDownPiece = xDiagonally + xDirection;
        int yBehindKnockDownPiece = yDiagonally + yDirection;
        int steps = determineTheNumberOfIterationsOfTheLoop(selectedChecker, coordinate);
        Checker knockedDownChecker = null;
        for (int i = 0; i < steps; i++) {
            if (!(checkIfCoordinatesAreInBoard(xDiagonally, yDiagonally))
                    || !(checkIfCoordinatesAreInBoard(xBehindKnockDownPiece, yBehindKnockDownPiece))) {
                break;
            }
            if (BOARD.getBoard()[yDiagonally][xDiagonally] != null
                    && BOARD.getBoard()[yBehindKnockDownPiece][xBehindKnockDownPiece] != null) {
                break;
            }
            if (BOARD.getBoard()[yDiagonally][xDiagonally] == null) {
                xDiagonally += xDirection;
                yDiagonally += yDirection;
                xBehindKnockDownPiece += xDirection;
                yBehindKnockDownPiece += yDirection;
                continue;
            }
            if (BOARD.getBoard()[yDiagonally][xDiagonally] != null
                    && BOARD.getBoard()[yBehindKnockDownPiece][xBehindKnockDownPiece] == null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yDiagonally][xDiagonally].getColor().getColorGroup()))) {
                knockedDownChecker = BOARD.getBoard()[yDiagonally][xDiagonally];
                break;
            }
        }
        return knockedDownChecker;
    }


    private int  determineTheNumberOfIterationsOfTheLoop(Checker selectedChecker, Coordinate coordinate) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (coordinate == null) {
            throw new CoordinateAreNullException("Coordinate cannot be null!");
        }
        int xSteps = Math.abs(coordinate.getX() - selectedChecker.getCoordinate().getX());
        int ySteps = Math.abs(coordinate.getY() - selectedChecker.getCoordinate().getY());
        return Math.max(xSteps, ySteps);
    }


    public Checker getQueenIfItCanMove(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (getQueenIfItCanMoveWithBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveWithBeating(selectedChecker);
        } else if (getQueenIfItCanMoveWithoutBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveWithoutBeating(selectedChecker);
        }
        return null;
    }


    public Checker getQueenIfItCanMoveWithBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (getQueenIfItCanMoveToTheTopLeftCornerWithBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheTopLeftCornerWithBeating(selectedChecker);
        }
        if (getQueenIfItCanMoveToTheBottomLeftCornerWithBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheBottomLeftCornerWithBeating(selectedChecker);
        }
        if (getQueenIfItCanMoveToTheTopRightCornerWithBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheTopRightCornerWithBeating(selectedChecker);
        }
        if (getQueenIfItCanMoveToTheBottomRightCornerWithBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheBottomRightCornerWithBeating(selectedChecker);
        }
        return null;
    }


    public Checker getQueenIfItCanMoveWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (getQueenIfItCanMoveToTheTopLeftCornerWithoutBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheTopLeftCornerWithoutBeating(selectedChecker);
        }
        if (getQueenIfItCanMoveToTheBottomLeftCornerWithoutBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheBottomLeftCornerWithoutBeating(selectedChecker);
        }
        if (getQueenIfItCanMoveToTheTopRightCornerWithoutBeating(selectedChecker) != null) {
            return getQueenIfItCanMoveToTheTopRightCornerWithoutBeating(selectedChecker);
        }
        if (getQueenIfItCanMoveToTheBottomRightCornerWithoutBeating(selectedChecker) != null) {
            getQueenIfItCanMoveToTheBottomRightCornerWithoutBeating(selectedChecker);
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheTopLeftCornerWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            Coordinate leftUp = new Coordinate(x - coordinateValue, y + coordinateValue);
            if (checkIfCoordinatesAreInBoard(leftUp.getX(), leftUp.getY())) {
                Checker possibleMoveLeftUp = BOARD.getBoard()[leftUp.getY()][leftUp.getX()];
                if (possibleMoveLeftUp == null) {
                    return selectedChecker;
                }
            }
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheBottomLeftCornerWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            Coordinate leftDown = new Coordinate(x - coordinateValue, y - coordinateValue);
            if (checkIfCoordinatesAreInBoard(leftDown.getX(), leftDown.getY())) {
                Checker possibleMoveLeftDown = BOARD.getBoard()[leftDown.getY()][leftDown.getX()];
                if (possibleMoveLeftDown == null) {
                    return selectedChecker;
                }
            }
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheTopRightCornerWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            Coordinate rightUp = new Coordinate(x + coordinateValue, y + coordinateValue);
            if (checkIfCoordinatesAreInBoard(rightUp.getX(), rightUp.getY())) {
                Checker possibleMoveRightUp = BOARD.getBoard()[rightUp.getY()][rightUp.getX()];
                if (possibleMoveRightUp == null) {
                    return selectedChecker;
                }
            }
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheBottomRightCornerWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 0; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            Coordinate rightDown = new Coordinate(x + coordinateValue, y - coordinateValue);
            if (checkIfCoordinatesAreInBoard(rightDown.getX(), rightDown.getY())) {
                Checker possibleMoveRightDown = BOARD.getBoard()[rightDown.getY()][rightDown.getX()];
                if (possibleMoveRightDown == null) {
                    return selectedChecker;
                }
            }
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheTopLeftCornerWithBeating
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xTopLeftCorner = x - coordinateValue;
            int yTopLeftCorner = y + coordinateValue;
            int moveOneField = 1;
            int xBehindTopLeftCorner = xTopLeftCorner - moveOneField;
            int yBehindTopLeftCorner = yTopLeftCorner + moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xTopLeftCorner, yTopLeftCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindTopLeftCorner, yBehindTopLeftCorner))) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && BOARD.getBoard()[yBehindTopLeftCorner][xBehindTopLeftCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindTopLeftCorner][xBehindTopLeftCorner] == null) {
                return selectedChecker;
            }
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheBottomLeftCornerWithBeating
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xBottomLeftCorner = x - coordinateValue;
            int yBottomLeftCorner = y - coordinateValue;
            int moveOneField = 1;
            int xBehindBottomLeftCorner = xBottomLeftCorner - moveOneField;
            int yBehindBottomLeftCorner = yBottomLeftCorner - moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xBottomLeftCorner, yBottomLeftCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindBottomLeftCorner, yBehindBottomLeftCorner))) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && BOARD.getBoard()[yBehindBottomLeftCorner][xBehindBottomLeftCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindBottomLeftCorner][xBehindBottomLeftCorner] == null) {
                return selectedChecker;
            }

        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheTopRightCornerWithBeating
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xTopRightCorner = x + coordinateValue;
            int yTopRightCorner = y + coordinateValue;
            int moveOneField = 1;
            int xBehindTopRightCorner = xTopRightCorner + moveOneField;
            int yBehindTopRightCorner = yTopRightCorner + moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xTopRightCorner, yTopRightCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindTopRightCorner, yBehindTopRightCorner))) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && BOARD.getBoard()[yBehindTopRightCorner][xBehindTopRightCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopRightCorner][xTopRightCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopRightCorner][xTopRightCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindTopRightCorner][xBehindTopRightCorner] == null) {
                return selectedChecker;
            }
        }
        return null;
    }


    private Checker getQueenIfItCanMoveToTheBottomRightCornerWithBeating
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xBottomRightCorner = x + coordinateValue;
            int yBottomRightCorner = y - coordinateValue;
            int moveOneField = 1;
            int xBehindBottomRightCorner = xBottomRightCorner + moveOneField;
            int yBehindBottomRightCorner = yBottomRightCorner - moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xBottomRightCorner, yBottomRightCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindBottomRightCorner, yBehindBottomRightCorner))) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && BOARD.getBoard()[yBehindBottomRightCorner][xBehindBottomRightCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindBottomRightCorner][xBehindBottomRightCorner] == null) {
                return selectedChecker;
            }
        }
        return null;
    }


    public List<Coordinate> getAvailableMovesForQueenWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        List<Coordinate> topLeftCornerWithoutBeating =
                getAvailableMovesForQueenIfItCanMoveToTheTopLeftCorner(selectedChecker);
        List<Coordinate> bottomLeftCornerWithoutBeating =
                getAvailableMovesForQueenIfItCanMoveToTheBottomLeftCorner(selectedChecker);
        List<Coordinate> topRightCornerWithoutBeating =
                getAvailableMovesForQueenIfItCanMoveToTheTopRightCorner(selectedChecker);
        List<Coordinate> bottomRightCornerWithoutBeating =
                getAvailableMovesForQueenIfItCanMoveToTheBottomRightCorner(selectedChecker);
        availableMoves.addAll(topLeftCornerWithoutBeating);
        availableMoves.addAll(bottomLeftCornerWithoutBeating);
        availableMoves.addAll(topRightCornerWithoutBeating);
        availableMoves.addAll(bottomRightCornerWithoutBeating);
        return availableMoves;
    }


    public List<Coordinate> getAvailableMovesForQueenWithThePossibilityBeatingChecker(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        List<Coordinate> topLeftCornerWithBeating =
                getAvailableMovesForQueenIfItCanMoveToTheTopLeftCornerWithThePossibilityBeatingChecker(selectedChecker);
        List<Coordinate> bottomLeftCornerWithBeating =
                getAvailableMovesForQueenIfItCanMoveToTheBottomLeftCornerWithThePossibilityBeatingChecker(selectedChecker);
        List<Coordinate> topRightCornerWithBeating =
                getAvailableMovesForQueenIfItCanMoveToTheTopRightCornerWithThePossibilityBeatingChecker(selectedChecker);
        List<Coordinate> bottomRightCornerWithBeating =
                getAvailableMovesForQueenIfItCanMoveToTheBottomRightCornerWithThePossibilityBeatingChecker(selectedChecker);
        availableMoves.addAll(topLeftCornerWithBeating);
        availableMoves.addAll(bottomLeftCornerWithBeating);
        availableMoves.addAll(topRightCornerWithBeating);
        availableMoves.addAll(bottomRightCornerWithBeating);
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheTopLeftCornerWithThePossibilityBeatingChecker
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xTopLeftCorner = x - coordinateValue;
            int yTopLeftCorner = y + coordinateValue;
            int moveOneField = 1;
            int xBehindTopLeftCorner = xTopLeftCorner - moveOneField;
            int yBehindTopLeftCorner = yTopLeftCorner + moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xTopLeftCorner, yTopLeftCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindTopLeftCorner, yBehindTopLeftCorner))) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && BOARD.getBoard()[yBehindTopLeftCorner][xBehindTopLeftCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindTopLeftCorner][xBehindTopLeftCorner] == null) {
                availableMoves.add(new Coordinate(xBehindTopLeftCorner, yBehindTopLeftCorner));
                break;
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheBottomLeftCornerWithThePossibilityBeatingChecker
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xBottomLeftCorner = x - coordinateValue;
            int yBottomLeftCorner = y - coordinateValue;
            int moveOneField = 1;
            int xBehindBottomLeftCorner = xBottomLeftCorner - moveOneField;
            int yBehindBottomLeftCorner = yBottomLeftCorner - moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xBottomLeftCorner, yBottomLeftCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindBottomLeftCorner, yBehindBottomLeftCorner))) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && BOARD.getBoard()[yBehindBottomLeftCorner][xBehindBottomLeftCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindBottomLeftCorner][xBehindBottomLeftCorner] == null) {
                availableMoves.add(new Coordinate(xBehindBottomLeftCorner, yBehindBottomLeftCorner));
                break;
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheTopRightCornerWithThePossibilityBeatingChecker
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xTopRightCorner = x + coordinateValue;
            int yTopRightCorner = y + coordinateValue;
            int moveOneField = 1;
            int xBehindTopRightCorner = xTopRightCorner + moveOneField;
            int yBehindTopRightCorner = yTopRightCorner + moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xTopRightCorner, yTopRightCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindTopRightCorner, yBehindTopRightCorner))) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && BOARD.getBoard()[yBehindTopRightCorner][xBehindTopRightCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopRightCorner][xTopRightCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yTopRightCorner][xTopRightCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindTopRightCorner][xBehindTopRightCorner] == null) {
                availableMoves.add(new Coordinate(xBehindTopRightCorner, yBehindTopRightCorner));
                break;
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheBottomRightCornerWithThePossibilityBeatingChecker
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xBottomRightCorner = x + coordinateValue;
            int yBottomRightCorner = y - coordinateValue;
            int moveOneField = 1;
            int xBehindBottomRightCorner = xBottomRightCorner + moveOneField;
            int yBehindBottomRightCorner = yBottomRightCorner - moveOneField;
            if (!(checkIfCoordinatesAreInBoard(xBottomRightCorner, yBottomRightCorner))
                    || !(checkIfCoordinatesAreInBoard(xBehindBottomRightCorner, yBehindBottomRightCorner))) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && BOARD.getBoard()[yBehindBottomRightCorner][xBehindBottomRightCorner] != null) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner].getColor().getColorGroup())) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && !(pieceIsInColorGroup
                    (selectedChecker.getColor().getColorGroup(), BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner].getColor().getColorGroup()))
                    && BOARD.getBoard()[yBehindBottomRightCorner][xBehindBottomRightCorner] == null) {
                availableMoves.add(new Coordinate(xBehindBottomRightCorner, yBehindBottomRightCorner));
                break;
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheTopLeftCorner(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xTopLeftCorner = x - coordinateValue;
            int yTopLeftCorner = y + coordinateValue;
            if (!checkIfCoordinatesAreInBoard(xTopLeftCorner, yTopLeftCorner)) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] != null
                    && !(BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner].getColor().equals(selectedChecker.getColor()))) {
                break;
            } else if (BOARD.getBoard()[yTopLeftCorner][xTopLeftCorner] == null) {
                availableMoves.add(new Coordinate(xTopLeftCorner, yTopLeftCorner));
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheBottomLeftCorner(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xBottomLeftCorner = x - coordinateValue;
            int yBottomLeftCorner = y - coordinateValue;
            if (!checkIfCoordinatesAreInBoard(xBottomLeftCorner, yBottomLeftCorner)) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] != null
                    && !(BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner].getColor().equals(selectedChecker.getColor()))) {
                break;
            } else if (BOARD.getBoard()[yBottomLeftCorner][xBottomLeftCorner] == null) {
                availableMoves.add(new Coordinate(xBottomLeftCorner, yBottomLeftCorner));
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheTopRightCorner(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xTopRightCorner = x + coordinateValue;
            int yTopRightCorner = y + coordinateValue;
            if (!checkIfCoordinatesAreInBoard(xTopRightCorner, yTopRightCorner)) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] != null
                    && !(BOARD.getBoard()[yTopRightCorner][xTopRightCorner].getColor().equals(selectedChecker.getColor()))) {
                break;
            } else if (BOARD.getBoard()[yTopRightCorner][xTopRightCorner] == null) {
                availableMoves.add(new Coordinate(xTopRightCorner, yTopRightCorner));
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getAvailableMovesForQueenIfItCanMoveToTheBottomRightCorner(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int x = selectedChecker.getCoordinate().getX();
        int y = selectedChecker.getCoordinate().getY();
        for (int coordinateValue = 1; coordinateValue < BOARD.getBoard().length; coordinateValue++) {
            int xBottomRightCorner = x + coordinateValue;
            int yBottomRightCorner = y - coordinateValue;
            if (!checkIfCoordinatesAreInBoard(xBottomRightCorner, yBottomRightCorner)) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] != null
                    && !(BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner].getColor().equals(selectedChecker.getColor()))) {
                break;
            } else if (BOARD.getBoard()[yBottomRightCorner][xBottomRightCorner] == null) {
                availableMoves.add(new Coordinate(xBottomRightCorner, yBottomRightCorner));
            }
        }
        return availableMoves;
    }


    @Override
    public boolean checkIfCoordinatesAreInBoard(int x, int y) {
        return x >= 0 && x < BOARD.getCols() && y >= 0 && y < BOARD.getRows();
    }


    @Override
    public boolean pieceIsInColorGroup(String firstPieceGroup, String secondPieceGroup) {
        if (firstPieceGroup == null || firstPieceGroup.isEmpty() || firstPieceGroup.isBlank()) {
            throw new ColorGroupCannotBeNullOrEmptyException("Color group cannot be empty");
        }
        if (secondPieceGroup == null || secondPieceGroup.isEmpty() || secondPieceGroup.isBlank()) {
            throw new ColorGroupCannotBeNullOrEmptyException("Color group cannot be empty");
        }
        return firstPieceGroup.equals(secondPieceGroup);
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


}
