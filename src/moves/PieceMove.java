package moves;

import exceptions.CheckerIsNullException;
import exceptions.CoordinateAreNullException;
import game.Board;
import game.Coordinate;
import piece.Checker;
import piece.Color;


import java.util.ArrayList;
import java.util.List;

public class PieceMove implements Move {

    private static final Board BOARD = Board.getInstance();


    public Checker getPieceIfYouCanMoveIt(Checker selectedPChecker) {
        if (selectedPChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (getPieceIfItCanMoveForwardAndBackwardWithBeating(selectedPChecker) != null) {
            return getPieceIfItCanMoveForwardAndBackwardWithBeating(selectedPChecker);
        } else {
            return getPieceWhichCanBeMovedForwardWithoutBeating(selectedPChecker);
        }
    }


    private Checker getPieceIfItCanMoveForwardAndBackwardWithBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (getPieceWhichCanBeMovedForwardWithBeating(selectedChecker) != null) {
            return getPieceWhichCanBeMovedForwardWithBeating(selectedChecker);
        }
        if (getPieceWhichCanBeMovedBackwardWithBeating(selectedChecker) != null) {
            return getPieceWhichCanBeMovedBackwardWithBeating(selectedChecker);
        }
        return null;
    }


    public Checker getPieceWhichCanBeMovedForwardWithoutBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int forwardVertical = selectedChecker.getColor().equals(Color.WHITE) ? 1 : -1;
        for (int forwardHorizontal = -1; forwardHorizontal <= 1; forwardHorizontal += 2) {
            int yForward = selectedChecker.getCoordinate().getY() + forwardVertical;
            int xForward = selectedChecker.getCoordinate().getX() + forwardHorizontal;
            if (checkIfCoordinatesAreInBoard(xForward, yForward)) {
                Checker possiblePieceBehind = BOARD.getBoard()[yForward][xForward];
                if (possiblePieceBehind == null) {
                    return selectedChecker;
                }
            }
        }
        return null;
    }


    public Checker getPieceWhichCanBeMovedForwardWithBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int forwardVertical = selectedChecker.getColor().equals(Color.WHITE) ? 1 : -1;
        for (int forwardHorizontal = -1; forwardHorizontal <= 1; forwardHorizontal += 2) {
            int yForward = selectedChecker.getCoordinate().getY() + forwardVertical;
            int xForward = selectedChecker.getCoordinate().getX() + forwardHorizontal;
            if (checkIfCoordinatesAreInBoard(xForward, yForward)) {
                Checker possiblePieceBehind = BOARD.getBoard()[yForward][xForward];
                if (possiblePieceBehind != null) {
                    int xBehindPossiblePiece = xForward + forwardHorizontal;
                    int yBehindPossiblePiece = yForward + forwardVertical;
                    if (checkMovementForBeating(possiblePieceBehind, selectedChecker, xBehindPossiblePiece, yBehindPossiblePiece)) {
                        return selectedChecker;
                    }
                }
            }
        }
        return null;
    }


    public Checker getPieceWhichCanBeMovedBackwardWithBeating(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int backwardVertical = selectedChecker.getColor().equals(Color.WHITE) ? -1 : 1;
        for (int backwardHorizontal = -1; backwardHorizontal <= 1; backwardHorizontal += 2) {
            int yBackward = selectedChecker.getCoordinate().getY() + backwardVertical;
            int xBackward = selectedChecker.getCoordinate().getX() + backwardHorizontal;
            if (checkIfCoordinatesAreInBoard(xBackward, yBackward)) {
                Checker possiblePieceBefore = BOARD.getBoard()[yBackward][xBackward];
                if (possiblePieceBefore != null) {
                    int xBeforePossiblePiece = xBackward + backwardHorizontal;
                    int yBeforePossiblePiece = yBackward + backwardVertical;
                    if (checkMovementForBeating(possiblePieceBefore, selectedChecker, xBeforePossiblePiece, yBeforePossiblePiece)) {
                        return selectedChecker;
                    }
                }
            }
        }
        return null;
    }


    public List<Coordinate> getListWithAvailableMovesForPiece(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        List<Coordinate> availableMovesForwardWithThePossibilityOfBeating =
                getListWithAvailableMovesForPieceForwardWithThePossibilityOfBeatingChecker(selectedChecker);
        List<Coordinate> availableMovesBackwardWithThePossibilityOfBeating =
                getListWithAvailableMovesForPieceBackwardWithThePossibilityOfBeatingChecker(selectedChecker);
        List<Coordinate> availableMovesForward = getListWithAvailableMovesForPieceForward(selectedChecker);
        if (!(availableMovesForwardWithThePossibilityOfBeating.isEmpty())
                || !(availableMovesBackwardWithThePossibilityOfBeating.isEmpty())) {
            availableMoves.addAll(availableMovesForwardWithThePossibilityOfBeating);
            availableMoves.addAll(availableMovesBackwardWithThePossibilityOfBeating);
        } else {
            availableMoves.addAll(availableMovesForward);
        }
        return availableMoves;
    }


    private List<Coordinate> getListWithAvailableMovesForPieceForward(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int forwardVertical = selectedChecker.getColor().equals(Color.WHITE) ? 1 : -1;
        for (int forwardHorizontal = -1; forwardHorizontal <= 1; forwardHorizontal += 2) {
            int yForward = selectedChecker.getCoordinate().getY() + forwardVertical;
            int xForward = selectedChecker.getCoordinate().getX() + forwardHorizontal;
            if (checkIfCoordinatesAreInBoard(xForward, yForward)) {
                Checker possiblePieceBehind = BOARD.getBoard()[yForward][xForward];
                if (possiblePieceBehind == null) {
                    availableMoves.add(new Coordinate(xForward, yForward));
                }
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getListWithAvailableMovesForPieceForwardWithThePossibilityOfBeatingChecker
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int forwardVertical = selectedChecker.getColor().equals(Color.WHITE) ? 1 : -1;
        for (int forwardHorizontal = -1; forwardHorizontal <= 1; forwardHorizontal += 2) {
            int yForward = selectedChecker.getCoordinate().getY() + forwardVertical;
            int xForward = selectedChecker.getCoordinate().getX() + forwardHorizontal;
            if (checkIfCoordinatesAreInBoard(xForward, yForward)) {
                Checker possiblePieceBehind = BOARD.getBoard()[yForward][xForward];
                if (possiblePieceBehind != null) {
                    int xBehindPossiblePiece = xForward + forwardHorizontal;
                    int yBehindPossiblePiece = yForward + forwardVertical;
                    if (checkMovementForBeating(possiblePieceBehind, selectedChecker, xBehindPossiblePiece, yBehindPossiblePiece)) {
                        availableMoves.add(new Coordinate(xBehindPossiblePiece, yBehindPossiblePiece));
                    }
                }
            }
        }
        return availableMoves;
    }


    private List<Coordinate> getListWithAvailableMovesForPieceBackwardWithThePossibilityOfBeatingChecker
            (Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMoves = new ArrayList<>();
        int backwardVertical = selectedChecker.getColor().equals(Color.WHITE) ? -1 : 1;
        for (int backwardHorizontal = -1; backwardHorizontal <= 1; backwardHorizontal += 2) {
            int yBackward = selectedChecker.getCoordinate().getY() + backwardVertical;
            int xBackward = selectedChecker.getCoordinate().getX() + backwardHorizontal;
            if (checkIfCoordinatesAreInBoard(xBackward, yBackward)) {
                Checker possiblePieceBefore = BOARD.getBoard()[yBackward][xBackward];
                if (possiblePieceBefore != null) {
                    int xBeforePossiblePiece = xBackward + backwardHorizontal;
                    int yBeforePossiblePiece = yBackward + backwardVertical;
                    if (checkMovementForBeating(possiblePieceBefore, selectedChecker, xBeforePossiblePiece, yBeforePossiblePiece)) {
                        availableMoves.add(new Coordinate(xBeforePossiblePiece, yBeforePossiblePiece));
                    }
                }
            }
        }
        return availableMoves;
    }


    public Checker getPieceThatThePieceHasKnockedDown(Checker selectedChecker, Coordinate newFieldCoordinate) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (newFieldCoordinate == null) {
            throw new CoordinateAreNullException("New Coordinate cannot be null!");
        }
        int x = (selectedChecker.getCoordinate().getX() + newFieldCoordinate.getX()) / 2;
        int y = (selectedChecker.getCoordinate().getY() + newFieldCoordinate.getY()) / 2;
        Checker checkPiece = BOARD.getBoard()[y][x];
        Checker knockedDownPiece;
        if (BOARD.getBoard()[y][x] != null
                && !(pieceIsInColorGroup
                (selectedChecker.getColor().getColorGroup(), checkPiece.getColor().getColorGroup()))
                && checkIfCoordinatesAreInBoard(x, y)) {
            knockedDownPiece = checkPiece;
            return knockedDownPiece;
        }
        return null;
    }


    private boolean checkMovementForBeating
            (Checker possiblePiece, Checker selectedChecker, int xPossiblePiece, int yPossiblePiece) {
        if (possiblePiece == null) {
            throw new CheckerIsNullException("Possible checker cannot be null!");
        }
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        return checkIfCoordinatesAreInBoard(xPossiblePiece, yPossiblePiece)
                && BOARD.getBoard()[yPossiblePiece][xPossiblePiece] == null
                && !pieceIsInColorGroup
                (selectedChecker.getColor().getColorGroup(), possiblePiece.getColor().getColorGroup());
    }


    @Override
    public boolean checkIfCoordinatesAreInBoard(int x, int y) {
        return x >= 0 && x < BOARD.getCols() && y >= 0 && y < BOARD.getRows();
    }


    @Override
    public boolean pieceIsInColorGroup(String firstPieceGroup, String secondPieceGroup) {
        return firstPieceGroup.equals(secondPieceGroup);
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


}
