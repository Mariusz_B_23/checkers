package moves;

import exceptions.*;
import game.Board;
import game.Coordinate;
import piece.BlackQueen;
import piece.Checker;
import piece.Color;
import piece.WhiteQueen;
import player.Player;

import java.util.ArrayList;
import java.util.List;

public class MoveManager {

    private static final Board BOARD = Board.getInstance();
    private static final QueenMove QUEEN_MOVE = new QueenMove();
    private static final PieceMove PIECE_MOVE = new PieceMove();


    public List<Checker> getListOfPiecesThatCanBeUsedForMovement(Player player) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        List<Checker> availableMoves = new ArrayList<>();
        for (Checker[] checkers : BOARD.getBoard()) {
            for (Checker checker : checkers) {
                if (checker != null
                        && (player.getColor().equals(checker.getColor())
                        || pieceIsInColorGroup(checker.getColor().getColorGroup(), player.getColor().getColorGroup()))) {
                    if (getPieceWhichCanBeMoved(checker) != null) {
                        availableMoves.add(checker);
                    }
                }
            }
        }
        return availableMoves;
    }


    public List<Checker> listWithPossibleBeating(List<Checker> availableCheckersToMove) {
        List<Checker> listWithBeating = new ArrayList<>();
        for (Checker piece : availableCheckersToMove) {
            if (piece.isQueen() && QUEEN_MOVE.getQueenIfItCanMoveWithBeating(piece) != null) {
                listWithBeating.add(piece);
            }
            if (!(piece.isQueen()) && PIECE_MOVE.getPieceWhichCanBeMovedForwardWithBeating(piece) != null) {
                listWithBeating.add(piece);
            }
            if (!(piece.isQueen()) && PIECE_MOVE.getPieceWhichCanBeMovedBackwardWithBeating(piece) != null) {
                listWithBeating.add(piece);
            }
        }
        return listWithBeating;
    }


    public List<Checker> listWithoutBeating(List<Checker> availableCheckersToMove) {
        List<Checker> listWithoutBeating = new ArrayList<>();
        for (Checker piece : availableCheckersToMove) {
            if (piece.isQueen() && QUEEN_MOVE.getQueenIfItCanMoveWithoutBeating(piece) != null) {
                listWithoutBeating.add(piece);
            }
            if (!(piece.isQueen()) && PIECE_MOVE.getPieceWhichCanBeMovedForwardWithoutBeating(piece) != null) {
                listWithoutBeating.add(piece);
            }
        }
        return listWithoutBeating;
    }


    public List<Checker> listWithPossibleCheckerMove(List<Checker> availableCheckersToMove) {
        List<Checker> withBeating = listWithPossibleBeating(availableCheckersToMove);
        List<Checker> withoutBeating = listWithoutBeating(availableCheckersToMove);
        List<Checker> moveList = new ArrayList<>(withBeating);
        if (withBeating.isEmpty()) {
            moveList.addAll(withoutBeating);
        }
        return moveList;
    }


    public void swappingAPawnForAQueen(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        int checkedRowForWhiteQueen = 7;
        int checkedRowForBlackQueen = 0;
        if (checkingThePieceToQueenRow(selectedChecker, checkedRowForWhiteQueen)
                && selectedChecker.getColor().getColorGroup().equals(Color.WHITE_QUEEN.getColorGroup())) {
            BOARD.getBoard()[selectedChecker.getCoordinate().getY()][selectedChecker.getCoordinate().getX()] =
                    new WhiteQueen(selectedChecker.getCoordinate());
        } else if (checkingThePieceToQueenRow(selectedChecker, checkedRowForBlackQueen)
                && selectedChecker.getColor().getColorGroup().equals(Color.BLACK_QUEEN.getColorGroup())) {
            BOARD.getBoard()[selectedChecker.getCoordinate().getY()][selectedChecker.getCoordinate().getX()] =
                    new BlackQueen(selectedChecker.getCoordinate());
        }
    }


    public void showListWithAvailableMovesForPiece(List<Coordinate> pieces) {
        if (pieces.isEmpty()) {
            throw new ListWithCoordinatesCannotBeEmptyException("List with coordinates cannot be empty.");
        }
        System.out.println("AVAILABLE MOVEMENT OPTIONS");
        for (Coordinate coordinate : pieces) {
            System.out.print(coordinate + "\t");
        }
        System.out.println();
    }


    private boolean pieceIsInColorGroup(String firstPieceGroup, String secondPieceGroup) {
        if (firstPieceGroup == null || firstPieceGroup.isEmpty() || firstPieceGroup.isBlank()) {
            throw new ColorGroupCannotBeNullOrEmptyException("Color group cannot be empty");
        }
        if (secondPieceGroup == null || secondPieceGroup.isEmpty() || secondPieceGroup.isBlank()) {
            throw new ColorGroupCannotBeNullOrEmptyException("Color group cannot be empty");
        }
        return firstPieceGroup.equals(secondPieceGroup);
    }


    private Checker getPieceWhichCanBeMoved(Checker selectedChecker) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (selectedChecker.isQueen()) {
            return QUEEN_MOVE.getQueenIfItCanMove(selectedChecker);
        } else {
            return PIECE_MOVE.getPieceIfYouCanMoveIt(selectedChecker);
        }
    }


    public void showListOfPiecesWhichCanMove(Player player, List<Checker> checkers) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null");
        }
        if (checkers.isEmpty()) {
            throw new ListWithCheckersCannotBeEmptyException("List with checkers cannot be empty.");
        }
        System.out.printf("""           
                ----------------------------------------------------------------------------------------------------------------
                                
                %s - %s here are your checkers with which to move
                """, player.getColor().name(), player.getName().toUpperCase());
        for (Checker piece : checkers) {
            if (piece.isQueen()
                    && piece.getColor().equals(Color.WHITE_QUEEN)
                    || piece.getColor().equals(Color.BLACK_QUEEN)) {
                System.out.print(piece.getCoordinate() + "(Q)" + "\t");
            } else if (!(piece.isQueen())
                    && piece.getColor().equals(Color.WHITE)
                    || piece.getColor().equals(Color.BLACK)) {
                System.out.print(piece.getCoordinate() + "\t");
            }
        }
        System.out.println();
    }


    public void movePieceOnTheBoard(Checker checker, Coordinate coordinate, Checker kncokedDownChecker) {
        if (checker == null) {
            throw new CheckerIsNullException("Checker cannot be null!");
        }
        if (coordinate == null) {
            throw new CoordinateAreNullException("Coordinate cannot be null!");
        }
        deletePieceFromBoard(checker);
        addPieceToBoard(checker, coordinate);
        if (kncokedDownChecker != null) {
            deletePieceFromBoard(kncokedDownChecker);
        }
    }


    private void deletePieceFromBoard(Checker checker) {
        if (checker == null) {
            throw new CheckerIsNullException("Checker cannot be null!");
        }
        BOARD.getBoard()[checker.getCoordinate().getY()][checker.getCoordinate().getX()] = null;
    }


    private void addPieceToBoard(Checker selectedChecker, Coordinate newCoordinate) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (newCoordinate == null) {
            throw new CoordinateAreNullException("New Coordinate cannot be null!");
        }
        selectedChecker.setCoordinate(newCoordinate);
        BOARD.getBoard()[newCoordinate.getY()][newCoordinate.getX()] = selectedChecker;
    }


    private boolean checkingThePieceToQueenRow(Checker selectedChecker, int rowNumber) {
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        if (rowNumber != 0 && rowNumber != 7) {
            throw new RowNumberIsIncorrectException("The row number cannot be other than 0 or 7.");
        }
        return (selectedChecker.getCoordinate().getY() == rowNumber);
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


}
