package moves;

public interface Move {


    boolean checkIfCoordinatesAreInBoard(int x, int y);


    boolean pieceIsInColorGroup(String firstPieceGroup, String secondPieceGroup);


}
