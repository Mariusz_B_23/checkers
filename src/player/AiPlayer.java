package player;

import exceptions.TheListSizeMustNotBeLessThanZeroException;
import game.Coordinate;
import piece.Checker;

import java.util.List;
import java.util.Random;

public class AiPlayer {

    private static final Random RANDOM = new Random();
    private static final String AI_NAME = "AI-VORY";


    public String getName() {
        return AI_NAME;
    }


    public void aiThinksAboutHisMovement() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private static int generate(int listSize) {
        if (listSize < 0) {
            throw new TheListSizeMustNotBeLessThanZeroException("The list size must not be less than zero.");
        }
        return RANDOM.nextInt(listSize) + 1;
    }


    public Checker aiChooseMove(List<Checker> listWithAiPossibleMoves) {
        int listSize = listWithAiPossibleMoves.size();
        int drawnNumber = generate(listSize);
        return listWithAiPossibleMoves.get(drawnNumber - 1);
    }


    public Coordinate aiChooseCoordinate(List<Coordinate> listWithAiPossibleCoordinates) {
        int listSize = listWithAiPossibleCoordinates.size();
        int drawnNumber = generate(listSize);
        return listWithAiPossibleCoordinates.get(drawnNumber - 1);
    }


}
