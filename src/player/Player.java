package player;

import exceptions.CheckerIsNullException;
import exceptions.CoordinateAreNullException;
import exceptions.UserNameCannotBeNullOrEmptyException;
import game.Board;
import game.Coordinate;
import piece.Color;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import piece.*;

public class Player {

    private final String name;
    private Color color;
    private static final Scanner KEYBOARD = new Scanner(System.in);
    private static final Board BOARD = Board.getInstance();


    public Player(String name, Color color) {
        this.name = name;
        this.color = color;
    }


    public String getName() {
        return name;
    }


    public Color getColor() {
        return color;
    }


    public void setColor(Color color) {
        this.color = color;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name) && color == player.color;
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, color);
    }


    @Override
    public String toString() {
        return name;
    }


    public static Player createWithNameAndCheckerColor() {
        String name = enterHisName();
        Color color = chooseCheckerColor(name);
        return new Player(name, color);
    }


    public static String enterHisName() {
        System.out.println("Please tell me your name:");
        String name;
        do {
            name = KEYBOARD.nextLine().toUpperCase();
            if (name.isEmpty()) {
                System.out.println("The name cannot be empty. Please once again enter your name...");
            }
        } while (name.isEmpty());
        return name.toUpperCase();
    }


    private static Color chooseCheckerColor(String name) {
        if (name == null || name.isEmpty() || name.isBlank()) {
            throw new UserNameCannotBeNullOrEmptyException("Color group cannot be empty");
        }
        String chooseColor;
        String regex = "([wb])";
        do {
            System.out.printf("""

                    %s please, enter the correct letter and choose the colour of the checkers you wish to play with..
                    W - white
                    B - black
                    """, name);
            chooseColor = KEYBOARD.nextLine().toLowerCase();
            if (!(chooseColor.matches(regex))) {
                System.out.println("Choose either the letter W or B.");
            }
        } while (!(chooseColor.matches(regex)));
        return chooseColor.equals(Color.WHITE.getDescription()) ? Color.WHITE : Color.BLACK;
    }


    public void informationOnTheCheckerThatWasChosen(Checker checker) {
        if (checker == null) {
            throw new CheckerIsNullException("Checker cannot be null!");
        }
        System.out.printf("""
                The chosen checker is: %s
                --------------------------------------------------------------------------------------------------------
                                
                """, checker.getCoordinate().toString());
    }


    public void informationOnTheFieldThatWasChosen(Coordinate coordinate) {
        if (coordinate == null) {
            throw new CoordinateAreNullException("Coordinate cannot be null!");
        }
        System.out.printf("""
                The chosen checker is: %s
                --------------------------------------------------------------------------------------------------------
                                
                """, coordinate);
    }


    public Checker chooseAPieceToMove(List<Checker> availableCheckers) {
        Checker userSelectedPiece;
        do {
            System.out.println("\nSELECT A CHECKER TO MOVE IT...");
            int column = userEntersTheLetterOfTheColumnOfHisChoice();
            int row = userEntersTheNumberOfTheRowOfHisChoice();
            Coordinate userCoordinate = new Coordinate(column, row);
            userSelectedPiece = getUserSelectedPiece(userCoordinate);
            if (!(availableCheckers.contains(userSelectedPiece))) {
                System.out.println("\nThe selected checker is not on the list!\n");
            }
        } while (!(availableCheckers.contains(userSelectedPiece)));
        return userSelectedPiece;
    }


    private Checker getUserSelectedPiece(Coordinate userCoordinate) {
        if (userCoordinate == null) {
            throw new CoordinateAreNullException("Coordinate cannot be null!");
        }
        Checker userSelectedPiece = null;
        if (BOARD.getBoard()[userCoordinate.getY()][userCoordinate.getX()] != null) {
            Color color = BOARD.getBoard()[userCoordinate.getY()][userCoordinate.getX()].getColor();
            if (color.equals(Color.WHITE)) {
                userSelectedPiece = new White(userCoordinate);
            } else if (color.equals(Color.WHITE_QUEEN)) {
                userSelectedPiece = new WhiteQueen(userCoordinate);
            } else if (color.equals(Color.BLACK)) {
                userSelectedPiece = new Black(userCoordinate);
            } else if (color.equals(Color.BLACK_QUEEN)) {
                userSelectedPiece = new BlackQueen(userCoordinate);
            }
        }
        return userSelectedPiece;
    }


    public Coordinate chooseAPlaceInBoardToMove(List<Coordinate> availableFieldInBoard) {
        Coordinate userSelectedField;
        do {
            System.out.println("\nSELECT A PLACE TO MOVE IN BOARD...\n");
            int column = userEntersTheLetterOfTheColumnOfHisChoice();
            int row = userEntersTheNumberOfTheRowOfHisChoice();
            userSelectedField = new Coordinate(column, row);
            if (!(availableFieldInBoard.contains(userSelectedField))) {
                System.out.println("\nThe selected checker is not on the list!\n");
            }
        } while (!(availableFieldInBoard.contains(userSelectedField)));
        return userSelectedField;
    }


    private int userEntersTheLetterOfTheColumnOfHisChoice() {
        String regexColumn = "[A-H]";
        int column;
        char userChooseColumn;
        do {
            System.out.println("Choose a letter [e.g. A-H]");
            userChooseColumn = KEYBOARD.next().toUpperCase().charAt(0);
            column = userChooseColumn - 65;
            if (!(Character.toString(userChooseColumn).matches(regexColumn))) {
                System.out.println("""
                                                
                        There is no such column on the board!
                        Please choose correct column...
                        """);
            }
        } while (!(Character.toString(userChooseColumn).matches(regexColumn)));
        return column;
    }


    private int userEntersTheNumberOfTheRowOfHisChoice() {
        String regexRow = "[1-8]";
        int row;
        String userChooseRow;
        do {
            System.out.println("Choose a number [e.g. 1-8]");
            userChooseRow = KEYBOARD.next();
            if (!(userChooseRow.matches(regexRow))) {
                System.out.println("""
                                                
                        There is no such row on the board!
                        Please choose correct row...
                        """);
            }
        } while (!(userChooseRow.matches(regexRow)));
        row = Integer.parseInt(userChooseRow) - 1;
        return row;
    }


}
