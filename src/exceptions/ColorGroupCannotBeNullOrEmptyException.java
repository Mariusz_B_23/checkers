package exceptions;

public class ColorGroupCannotBeNullOrEmptyException extends RuntimeException {


    public ColorGroupCannotBeNullOrEmptyException(String message) {
        super(message);
    }


}