package exceptions;

public class StartingPlayerIsNotCorrectException extends RuntimeException {


    public StartingPlayerIsNotCorrectException(String message) {
        super(message);
    }


}