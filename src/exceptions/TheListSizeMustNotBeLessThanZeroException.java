package exceptions;

public class TheListSizeMustNotBeLessThanZeroException extends RuntimeException{


    public TheListSizeMustNotBeLessThanZeroException(String message) {
        super(message);
    }


}
