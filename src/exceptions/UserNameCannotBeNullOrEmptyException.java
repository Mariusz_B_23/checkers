package exceptions;

public class UserNameCannotBeNullOrEmptyException extends RuntimeException {


    public UserNameCannotBeNullOrEmptyException(String message) {
        super(message);
    }


}