package exceptions;

public class ListWithCheckersCannotBeEmptyException extends RuntimeException {


    public ListWithCheckersCannotBeEmptyException(String message) {
        super(message);
    }


}