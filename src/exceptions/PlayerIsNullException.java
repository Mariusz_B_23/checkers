package exceptions;

public class PlayerIsNullException extends RuntimeException {


    public PlayerIsNullException(String message) {
        super(message);
    }


}