package exceptions;

public class RowNumberIsIncorrectException extends RuntimeException {


    public RowNumberIsIncorrectException(String message) {
        super(message);
    }


}
