package exceptions;

public class CoordinateAreNullException extends RuntimeException {


    public CoordinateAreNullException(String message) {
        super(message);
    }


}