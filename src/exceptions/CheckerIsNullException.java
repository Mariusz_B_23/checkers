package exceptions;

public class CheckerIsNullException extends RuntimeException {


    public CheckerIsNullException(String message) {
        super(message);
    }


}
