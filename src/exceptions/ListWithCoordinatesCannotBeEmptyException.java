package exceptions;

public class ListWithCoordinatesCannotBeEmptyException extends RuntimeException {


    public ListWithCoordinatesCannotBeEmptyException(String message) {
        super(message);
    }


}