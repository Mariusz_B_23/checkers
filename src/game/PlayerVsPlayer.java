package game;

import exceptions.CheckerIsNullException;
import exceptions.PlayerIsNullException;
import exceptions.StartingPlayerIsNotCorrectException;
import piece.Checker;
import piece.Color;
import player.Player;

import java.util.ArrayList;
import java.util.List;


public class PlayerVsPlayer extends Gameplay {


    @Override
    public void game() {
        System.out.println("""
                                
                We start the game PLAYER VS PLAYER.
                               
                """);
        player1 = createFirstPlayer();
        player2 = createSecondPlayer();
        String startingPlayer = player1.getColor() == Color.WHITE ? player1.getName() : player2.getName();
        startInfoAboutGame(startingPlayer);
        pause();
        boolean player1Status = getPlayerOneStatus();
        informationAboutTheWinner(player1Status);
    }


    @Override
    public boolean getPlayerOneStatus() {
        boolean player1Status = false;
        boolean player2Status = false;
        do {
            if (player1.getColor().equals(Color.WHITE)) {
                player1Status = runMatch(player1);
                if (!player1Status) {
                    player2Status = runMatch(player2);
                }
            } else {
                player2Status = runMatch(player2);
                if (!player2Status) {
                    player1Status = runMatch(player1);
                }
            }
        } while (!player1Status && !player2Status);
        return player1Status;
    }


    @Override
    public void informationAboutTheWinner(boolean player1Status) {
        if (player1Status) {
            System.out.printf("""
                            
                            
                    AND THE WINNER IS ... %S
                    """, player2.getName());
        } else {
            System.out.printf("""
                            
                            
                    AND THE WINNER IS ... %S
                    """, player1.getName());
        }
        pause();
        System.out.println("""
                    
                GAME OVER :D
                """);
    }


    @Override
    public void startInfoAboutGame(String startingPlayer) {
        if (startingPlayer == null || startingPlayer.isEmpty() || startingPlayer.isBlank()) {
            throw new StartingPlayerIsNotCorrectException("Starting player cannot be empty");
        }
        System.out.printf("""
                                        
                        First player: %s, checkers color: %s.
                        Second player: %s, checkers color: %s.
                                                
                        The game begins with %s as he has chosen the color white.
                                                
                        ------------------------------------------------------------------------------------------------
                                                
                        """, player1.getName(), player1.getColor().name(),
                player2.getName(), player2.getColor().name(), startingPlayer);
    }

    @Override
    public Player createFirstPlayer() {
        System.out.println("First player");
        return Player.createWithNameAndCheckerColor();
    }


    @Override
    public Player createSecondPlayer() {
        System.out.println("""
                        
                Second player""");
        String secondPlayerName = Player.enterHisName();
        return new Player(secondPlayerName,
                player1.getColor().equals(Color.WHITE) ? Color.BLACK : Color.WHITE);
    }


    @Override
    public void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public boolean runMatch(Player player) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        List<Checker> listWithPossibleCheckerMove = getListWithPossibleCheckerMove(player);
        if (listWithPossibleCheckerMove == null) {
            return true;
        }
        Checker selectedChecker = player.chooseAPieceToMove(listWithPossibleCheckerMove);
        player.informationOnTheCheckerThatWasChosen(selectedChecker);
        if (selectedChecker.isQueen()) {
            getActionWhenCheckerWhoChooseIsAQueen(player, selectedChecker);
        } else {
            getActionWhenCheckerWhoChooseIsANormalPiece(player, selectedChecker);
        }
        return false;
    }


    @Override
    public List<Checker> getListWithPossibleCheckerMove(Player player) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        Board.display();
        List<Checker> availablePiecesToMove = moveManager.getListOfPiecesThatCanBeUsedForMovement(player);
        if (availablePiecesToMove.isEmpty()) {
            return null;
        }
        List<Checker> listWithPossibleCheckerMove = moveManager.listWithPossibleCheckerMove(availablePiecesToMove);
        moveManager.showListOfPiecesWhichCanMove(player, listWithPossibleCheckerMove);
        return listWithPossibleCheckerMove;
    }


    @Override
    public void getActionWhenCheckerWhoChooseIsAQueen(Player player, Checker selectedChecker) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMovesForQueen =
                new ArrayList<>(queenMove.getAvailableMovesForQueenWithThePossibilityBeatingChecker(selectedChecker));
        if (availableMovesForQueen.isEmpty()) {
            availableMovesForQueen.addAll(queenMove.getAvailableMovesForQueenWithoutBeating(selectedChecker));
        }
        moveManager.showListWithAvailableMovesForPiece(availableMovesForQueen);
        Coordinate newFieldForQueen = player.chooseAPlaceInBoardToMove(availableMovesForQueen);
        player.informationOnTheFieldThatWasChosen(newFieldForQueen);
        Checker knockedPiece = queenMove.getPieceThatTheQueenHasKnockedDown(selectedChecker, newFieldForQueen);
        moveManager.movePieceOnTheBoard(selectedChecker, newFieldForQueen, knockedPiece);
    }


    @Override
    public void getActionWhenCheckerWhoChooseIsANormalPiece(Player player, Checker selectedChecker) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMovesForPiece = pieceMove.getListWithAvailableMovesForPiece(selectedChecker);
        moveManager.showListWithAvailableMovesForPiece(availableMovesForPiece);
        Coordinate newFieldForPiece = player.chooseAPlaceInBoardToMove(availableMovesForPiece);
        player.informationOnTheFieldThatWasChosen(newFieldForPiece);
        Checker knockedPiece = pieceMove.getPieceThatThePieceHasKnockedDown(selectedChecker, newFieldForPiece);
        moveManager.movePieceOnTheBoard(selectedChecker, newFieldForPiece, knockedPiece);
        moveManager.swappingAPawnForAQueen(selectedChecker);
    }


}
