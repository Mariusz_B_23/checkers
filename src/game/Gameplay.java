package game;

import moves.MoveManager;
import moves.PieceMove;
import moves.QueenMove;
import piece.Checker;
import player.Player;

import java.util.List;

public abstract class Gameplay {

    protected MoveManager moveManager = new MoveManager();
    protected QueenMove queenMove = new QueenMove();
    protected PieceMove pieceMove = new PieceMove();
    protected Player player1;
    protected Player player2;


    public abstract void game();


    public abstract boolean runMatch(Player player);


    public abstract List<Checker> getListWithPossibleCheckerMove(Player player);


    public abstract void getActionWhenCheckerWhoChooseIsAQueen(Player player, Checker selectedChecker);


    public abstract void getActionWhenCheckerWhoChooseIsANormalPiece(Player player, Checker selectedChecker);


    public abstract Player createFirstPlayer();


    public abstract Player createSecondPlayer();


    public abstract boolean getPlayerOneStatus();


    public abstract void startInfoAboutGame(String startingPlayer);


    public abstract void informationAboutTheWinner(boolean player1Status);


    public abstract void pause();


}


