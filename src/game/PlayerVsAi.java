package game;

import exceptions.CheckerIsNullException;
import exceptions.PlayerIsNullException;
import exceptions.StartingPlayerIsNotCorrectException;
import piece.Checker;
import piece.Color;
import player.AiPlayer;
import player.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerVsAi extends Gameplay {

    private final static AiPlayer AI_PLAYER = new AiPlayer();


    @Override
    public void game() {
        System.out.println("""
                                
                We start the game PLAYER VS AI.
                                
                """);
        player1 = createFirstPlayer();
        player2 = createSecondPlayer();
        artificialIntelligenceWelcomesThePlayer();
        String startingPlayer = player1.getColor() == Color.WHITE ? player1.getName() : AI_PLAYER.getName();
        startInfoAboutGame(startingPlayer);
        pause();
        boolean player1Status = getPlayerOneStatus();
        informationAboutTheWinner(player1Status);
    }


    @Override
    public boolean getPlayerOneStatus() {
        boolean player1Status = false;
        boolean player2Status = false;
        PlayerVsPlayer playerVsPlayer = new PlayerVsPlayer();
        do {
            if (player1.getColor().equals(Color.WHITE)) {
                player1Status = playerVsPlayer.runMatch(player1);
                if (!player1Status) {
                    player2Status = runMatch(player2);
                }
            } else {
                player2Status = runMatch(player2);
                if (!player2Status) {
                    player1Status = playerVsPlayer.runMatch(player1);
                }
            }
        } while (!player1Status && !player2Status);
        return player1Status;
    }


    @Override
    public void informationAboutTheWinner(boolean player1Status) {
        if (player1Status) {
            System.out.printf("""
                            
                            
                    AND THE WINNER IS ... %S
                    """, player2.getName());
        } else {
            System.out.printf("""
                            
                            
                    AND THE WINNER IS ... %S
                    """, player1.getName());
        }
        pause();
        System.out.println("""
                    
                GAME OVER :D
                """);
    }


    @Override
    public void startInfoAboutGame(String startingPlayer) {
        if (startingPlayer == null || startingPlayer.isEmpty() || startingPlayer.isBlank()) {
            throw new StartingPlayerIsNotCorrectException("Starting player cannot be empty");
        }
        System.out.printf("""
                        
                The game begins with %s as he has chosen the color white.
                                
                --------------------------------------------------------------------------------------------------------
                                
                """, startingPlayer);
    }


    private void artificialIntelligenceWelcomesThePlayer() {
        System.out.printf("""
                                
                Hi, my name is %s and I am an artificial intelligence and I will be playing with %s pawns.
                I'm happy that we can face each other playing Checkers.
                I will not sell my skin cheaply.
                Good luck my friend!
                """, AI_PLAYER.getName(), player2.getColor().name().toLowerCase());
    }


    @Override
    public void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public Player createFirstPlayer() {
        System.out.println("First player");
        return Player.createWithNameAndCheckerColor();
    }


    @Override
    public Player createSecondPlayer() {
        return new Player(AI_PLAYER.getName(), player1.getColor().equals(Color.WHITE) ? Color.BLACK : Color.WHITE);
    }


    @Override
    public boolean runMatch(Player player) {
        List<Checker> listWithPossibleCheckerMove = getListWithPossibleCheckerMove(player);
        if (listWithPossibleCheckerMove == null) {
            return true;
        }
        AI_PLAYER.aiThinksAboutHisMovement();
        Checker selectedChecker = AI_PLAYER.aiChooseMove(listWithPossibleCheckerMove);
        player.informationOnTheCheckerThatWasChosen(selectedChecker);
        if (selectedChecker.isQueen()) {
            getActionWhenCheckerWhoChooseIsAQueen(player, selectedChecker);
        } else {
            getActionWhenCheckerWhoChooseIsANormalPiece(player, selectedChecker);
        }
        return false;
    }


    @Override
    public List<Checker> getListWithPossibleCheckerMove(Player player) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        Board.display();
        List<Checker> availablePiecesToMove = moveManager.getListOfPiecesThatCanBeUsedForMovement(player);
        if (availablePiecesToMove.isEmpty()) {
            return null;
        }
        List<Checker> listWithPossibleCheckerMove = moveManager.listWithPossibleCheckerMove(availablePiecesToMove);
        moveManager.showListOfPiecesWhichCanMove(player, listWithPossibleCheckerMove);
        return listWithPossibleCheckerMove;
    }


    @Override
    public void getActionWhenCheckerWhoChooseIsAQueen(Player player, Checker selectedChecker) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMovesForQueen =
                new ArrayList<>(queenMove.getAvailableMovesForQueenWithThePossibilityBeatingChecker(selectedChecker));
        if (availableMovesForQueen.isEmpty()) {
            availableMovesForQueen.addAll(queenMove.getAvailableMovesForQueenWithoutBeating(selectedChecker));
        }
        moveManager.showListWithAvailableMovesForPiece(availableMovesForQueen);
        AI_PLAYER.aiThinksAboutHisMovement();
        Coordinate newFieldForQueen = AI_PLAYER.aiChooseCoordinate(availableMovesForQueen);
        player.informationOnTheFieldThatWasChosen(newFieldForQueen);
        AI_PLAYER.aiThinksAboutHisMovement();
        Checker knockedPiece = queenMove.getPieceThatTheQueenHasKnockedDown(selectedChecker, newFieldForQueen);
        moveManager.movePieceOnTheBoard(selectedChecker, newFieldForQueen, knockedPiece);
    }


    @Override
    public void getActionWhenCheckerWhoChooseIsANormalPiece(Player player, Checker selectedChecker) {
        if (player == null) {
            throw new PlayerIsNullException("Player cannot be null!");
        }
        if (selectedChecker == null) {
            throw new CheckerIsNullException("Selected checker cannot be null!");
        }
        List<Coordinate> availableMovesForPiece = pieceMove.getListWithAvailableMovesForPiece(selectedChecker);
        moveManager.showListWithAvailableMovesForPiece(availableMovesForPiece);
        AI_PLAYER.aiThinksAboutHisMovement();
        Coordinate newFieldForPiece = AI_PLAYER.aiChooseCoordinate(availableMovesForPiece);
        player.informationOnTheFieldThatWasChosen(newFieldForPiece);
        AI_PLAYER.aiThinksAboutHisMovement();
        Checker knockedPiece = pieceMove.getPieceThatThePieceHasKnockedDown(selectedChecker, newFieldForPiece);
        moveManager.movePieceOnTheBoard(selectedChecker, newFieldForPiece, knockedPiece);
        moveManager.swappingAPawnForAQueen(selectedChecker);
    }


}
