package game;

import java.util.Objects;

public class Coordinate {

    private final int x;
    private final int y;
    private final char column;
    private final char row;


    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
        this.column = (char) ('A' + x);
        this.row = (char) ('1' + y);
    }


    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x && y == that.y && column == that.column && row == that.row;
    }


    @Override
    public int hashCode() {
        return Objects.hash(x, y, column, row);
    }


    @Override
    public String toString() {
        return column + "" + row;
    }


}



