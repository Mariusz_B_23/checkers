package game;

import java.util.Objects;
import java.util.Scanner;

public class GameManager {

    private static final Scanner KEYBOARD = new Scanner(System.in);
    private static final Board BOARD = Board.getInstance();


    public static void windowWelcomeAndGameModeSelection() {
        System.out.println("\nWelcome to the CHECKERS game.");
        int gameMode = playerSelectTheGameMode();
        if (gameMode == 0) {
            System.out.println("Exiting the game. Come again!");
            return;
        }
        BOARD.setup();
        Gameplay gameplay = null;
        switch (gameMode) {
            case 1 -> gameplay = new PlayerVsAi();
            case 2 -> gameplay = new PlayerVsPlayer();
        }
        Objects.requireNonNull(gameplay).game();
    }


    private static int playerSelectTheGameMode() {
        String selectGameMode;
        String regex = "[0-2]";
        System.out.println("""
                                
                Please select the game mode:
                1 - PLAYER VS COM
                2 - PLAYER VS PLAYER
                0 - EXIT FROM THE GAME
                
                PLEASE SELECT THE OPTION NUMBER:
                """);
        do {
            selectGameMode = KEYBOARD.nextLine();
            if (!(selectGameMode.matches(regex))) {
                System.out.println("""
                                    
                        Select a number from the list.""");
            }
        } while (!(selectGameMode.matches(regex)));
        return Integer.parseInt(selectGameMode);
    }


}
