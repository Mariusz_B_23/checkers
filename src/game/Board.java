package game;

import piece.Black;
import piece.Checker;
import piece.White;

import java.util.StringJoiner;

public class Board {

    private final static int ROWS = 8;
    private final static int COLS = 8;
    private final static Checker[][] BOARD = new Checker[ROWS][COLS];
    private final static Board INSTANCE = new Board();


    private Board() {
    }


    public static Board getInstance() {
        return INSTANCE;
    }


    public Checker[][] getBoard() {
        return BOARD;
    }


    public int getRows() {
        return ROWS;
    }


    public int getCols() {
        return COLS;
    }


    public void setup() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < BOARD[i].length; j++) {
                if ((i + j) % 2 == 0) {
                    BOARD[i][j] = new White(new Coordinate(j, i));
                }
            }
        }
        for (int i = 5; i < BOARD.length; i++) {
            for (int j = 0; j < BOARD[i].length; j++) {
                if ((i + j) % 2 == 0) {
                    BOARD[i][j] = new Black(new Coordinate(j, i));
                }
            }
        }
    }


    public static void display() {
        attributionLettersOfAtoH();
        drawHorizontalLine();
        for (int i = BOARD.length - 1; i >= 0; i--) {
            System.out.print((i + 1) + "  | ");
            StringJoiner joiner = new StringJoiner(" | ");
            for (int j = 0; j < BOARD[i].length; j++) {
                joiner.add(BOARD[i][j] == null ? " " : BOARD[i][j].toString());
            }
            System.out.print(joiner);
            System.out.println(" |  " + (i + 1));
            if (i > 0) {
                for (int j = 0; j < joiner.toString().length() + 7; j++) {
                    if (j < 3) {
                        System.out.print(" ");
                    } else {
                        System.out.print("-");
                    }
                }
                System.out.println();
            }
        }
        drawHorizontalLine();
        attributionLettersOfAtoH();
    }


    private static void attributionLettersOfAtoH() {
        int i = 0;
        System.out.print("   ");
        for (int j = 0; j < BOARD[i].length; j++) {
            System.out.print("  " + (char) ('A' + j) + " ");
        }
        System.out.println();
    }


    private static void drawHorizontalLine() {
        for (int i = 0; i < 36; i++) {
            if (i < 3) {
                System.out.print(" ");
            } else {
                System.out.print("-");
            }
        }
        System.out.println();
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


}




