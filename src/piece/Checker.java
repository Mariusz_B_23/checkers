package piece;

import game.Coordinate;

import java.util.Objects;

public abstract class Checker {

    protected final Color color;
    protected Coordinate coordinate;
    protected final boolean isQueen;


    public Checker(Color color, Coordinate coordinate, boolean isQueen) {
        this.color = color;
        this.coordinate = coordinate;
        this.isQueen = isQueen;
    }


    public abstract Coordinate getCoordinate();


    public abstract void setCoordinate(Coordinate coordinate);


    public abstract Color getColor();


    public abstract boolean isQueen();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Checker checker = (Checker) o;
        return isQueen == checker.isQueen && color == checker.color && Objects.equals(coordinate, checker.coordinate);
    }


    @Override
    public int hashCode() {
        return Objects.hash(color, coordinate, isQueen);
    }


    @Override
    public String toString() {
        return String.valueOf(coordinate);
    }


}

