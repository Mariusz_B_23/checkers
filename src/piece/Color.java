package piece;

public enum Color {

    WHITE("w", "white"),
    BLACK("b", "black"),
    WHITE_QUEEN("W", "white"),
    BLACK_QUEEN("B", "black");

    private final String description;
    private final String colorGroup;


    Color(String description, String colorGroup) {
        this.description = description;
        this.colorGroup = colorGroup;
    }


    public String getDescription() {
        return description;
    }


    public String getColorGroup() {
        return colorGroup;
    }


    @Override
    public String toString() {
        return description;
    }


}
