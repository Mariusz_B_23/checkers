package piece;

import game.Coordinate;

public class WhiteQueen extends Checker {


    public WhiteQueen(Coordinate coordinate) {
        super(Color.WHITE_QUEEN, coordinate, true);
    }


    @Override
    public Coordinate getCoordinate() {
        return coordinate;
    }


    @Override
    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }


    @Override
    public Color getColor() {
        return color;
    }


    @Override
    public boolean isQueen() {
        return true;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


    @Override
    public String toString() {
        return String.valueOf(color);
    }


}
