package piece;

import game.Coordinate;

public class Black extends Checker {


    public Black(Coordinate coordinate) {
        super(Color.BLACK, coordinate, false);
    }


    @Override
    public Coordinate getCoordinate() {
        return coordinate;
    }


    @Override
    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }


    @Override
    public Color getColor() {
        return color;
    }


    @Override
    public boolean isQueen() {
        return false;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


    @Override
    public String toString() {
        return String.valueOf(color);
    }


}
